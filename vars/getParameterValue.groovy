String call(Map buildParams, String keyName){
   
    def defaultParams = getDefaultParams()

    if(env."${keyName}" != null ){
        println "ENV: для ключа ${keyName} найдено значение " +  env."${keyName}"
        return env."${keyName}"
    }else{
        if(buildParams."${keyName}" != null ){
            println "buildParams: для ключа ${keyName} найдено значение " +  buildParams."${keyName}"
            return buildParams."${keyName}"
        }
        if(defaultParams."${keyName}" != ''){
            println "defaultParams: для ключа ${keyName} найдено значение " +  defaultParams."${keyName}"
            return defaultParams."${keyName}"
        }
        println "Значение для ключа не найдено ${keyName} возвращаем пустую строку"
        return new String()
    }
}




