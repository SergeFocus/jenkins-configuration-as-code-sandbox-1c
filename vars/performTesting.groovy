#!/usr/bin/env groovy
def call(Map buildEnv){
    //env.RUNNER_IBNAME="/F${env.WORKSPACE}/build/ib"
    //        environment{
            def PATH_TO_TAMPLATE_BASE       = getParameterValue(buildEnv, 'PATH_TO_TAMPLATE_BASE')
//            def PATH_TO_TAMPLATE_BASE       = "./examples/demo.dt"
            def SOURCE_PATH                 = getParameterValue(buildEnv, 'SOURCE_PATH')
            def V8VERSION                   = getParameterValue(buildEnv, 'V8VERSION')
            def VRUNNER_CONF                = getParameterValue(buildEnv, 'VRUNNER_CONF')
            def _DB_USER_CREDENTIONALS_ID   = getParameterValue(buildEnv, 'DB_USER_CREDENTIONALS_ID')
            def PROCEDURE_SINTAX_CHECK      = getParameterValue(buildEnv, 'PROCEDURE_SINTAX_CHECK')
            def PROCEDURE_TDD_TEST          = getParameterValue(buildEnv, 'PROCEDURE_TDD_TEST')
            def PROCEDURE_BDD_TEST          = getParameterValue(buildEnv, 'PROCEDURE_BDD_TEST')
            def EMAILS_FOR_NOTIFICATION     = getParameterValue(buildEnv, 'EMAILS_FOR_NOTIFICATION')
            def scannerHome 
            def configurationText
            def configurationVersion
            def BIN_CATALOG = ''

            def ACC_PROPERTIES = ''
            def ACC_BASE = ''
            def ACC_USER = ''
            def CURRENT_CATALOG = ''
            def TEMP_CATALOG = ''
            def PROJECT_NAME = 'TEST'
            def PROJECT_KEY
            //def EDT_VALIDATION_RESULT = ''
            def GENERIC_ISSUE_JSON = ''
            def SRC = ''
            def PROJECT_URL = ''
            def sonar_catalog
            def git_repo_branch
            def ACC_recreateProject = false 
            def ACC_check = true



        //string(defaultValue: "${env.PROJECT_NAME}", description: '* Имя проекта. Одинаковое для EDT, проекта в АПК и в сонаре. Обычно совпадает с именем конфигурации.', name: 'PROJECT_NAME')
        //string(defaultValue: "${env.git_repo_url}", description: '* URL к гит-репозиторию, который необходимо проверить.', name: 'git_repo_url')
        //string(defaultValue: "${env.git_repo_branch}", description: 'Ветка репозитория, которую необходимо проверить. По умолчанию master', name: 'git_repo_branch')
        //string(defaultValue: "${env.sonar_catalog}", description: 'Каталог сонара, в котором лежит все, что нужно. По умолчанию C:/Sonar/', name: 'sonar_catalog')
        //string(defaultValue: "${env.PROPERTIES_CATALOG}", description: 'Каталог с настройками acc.properties, bsl-language-server.conf и sonar-project.properties. По умолчанию ./Sonar', name: 'PROPERTIES_CATALOG')
        //booleanParam(defaultValue: env.ACC_check== null ? true : env.ACC_check, description: 'Выполнять ли проверку АПК. Если нет, то будут получены существующие результаты. По умолчанию: true', name: 'ACC_check')
        //booleanParam(defaultValue: env.ACC_recreateProject== null ? false : env.ACC_recreateProject, description: 'Пересоздать проект в АПК. Все данные о проекте будут собраны заново. По умолчанию: false', name: 'ACC_recreateProject')
        //string(defaultValue: "${env.STEBI_SETTINGS}", description: 'Файл настроек для переопределения замечаний. Для файла из репо проекта должен начинатся с папки Repo, например .Repo/Sonar/settings.json. По умолчанию ./Sonar/settings.json', name: 'STEBI_SETTINGS')
        //string(defaultValue: "${env.jenkinsAgent}", description: 'Нода дженкинса, на которой запускать пайплайн. По умолчанию master', name: 'jenkinsAgent')
        //string(defaultValue: "${env.EDT_VERSION}", description: 'Используемая версия EDT. По умолчанию 1.13.0', name: 'EDT_VERSION')
        //string(defaultValue: "${env.perf_catalog}", description: 'Путь к каталогу с замерами производительности, на основе которых будет рассчитано покрытие. Если пусто - покрытие не считается.', name: 'perf_catalog')
        //string(defaultValue: "${env.git_credentials_Id}", description: 'ID Credentials для получения изменений из гит-репозитория', name: 'git_credentials_Id')
        //string(defaultValue: "${env.rocket_channel}", description: 'Канал в рокет-чате для отправки уведомлений', name: 'rocket_channel')

    def connectionString = getConnectionString(buildEnv)
 

        stage("Инициализация переменных") {

                        // Инициализация параметров значениями по умолчанию
                        sonar_catalog = "C:/Sonar"
                        PROPERTIES_CATALOG = "./Sonar"
                        
                        //EDT_VERSION = EDT_VERSION.isEmpty() ? '1.13.0' : EDT_VERSION
                        STEBI_SETTINGS = './Sonar/settings.json'
                        git_repo_branch = 'master'

                        //perf_catalog = perf_catalog == null || perf_catalog == 'null' ? '' : perf_catalog
                        
                        BIN_CATALOG = "${sonar_catalog}/bin/"
                        ACC_BASE = "${sonar_catalog}/acc/"
                        ACC_USER = 'Admin'
                        SRC = "/src/cf"

                        // Подготовка переменных по переданным параметрам
                        // Настройки инструментов
                        ACC_PROPERTIES = "./Repo/${PROPERTIES_CATALOG}/acc.properties"
                        if (fileExists(ACC_PROPERTIES)) {
                            echo "file exists: ${ACC_PROPERTIES}"
                        } else {
                            echo "file does not exist: ${ACC_PROPERTIES}"
                            ACC_PROPERTIES = "./Sonar/acc.properties"
                        }

                        
                        CURRENT_CATALOG = pwd()
                        TEMP_CATALOG = "${CURRENT_CATALOG}\\sonar_temp"


                        // создаем/очищаем временный каталог
                        dir(TEMP_CATALOG) {
                            deleteDir()
                            writeFile file: 'acc.json', text: '{"issues": []}'
                            writeFile file: 'bsl-generic-json.json', text: '{"issues": []}'
                            writeFile file: 'edt.json', text: '{"issues": []}'
                        }


                        
                        GENERIC_ISSUE_JSON ="${TEMP_CATALOG}/acc.json,${TEMP_CATALOG}/bsl-generic-json.json,${TEMP_CATALOG}/edt.json"

            
        }

            stage("Обновление тестового контура") {
                    timestamps {
                        script{
                            timeout(2000) {
                                prepareBase(buildEnv, connectionString) 
                            }
                        }
                            
                    }
            }

            stage('Синтаксическая проверка'){
                    timestamps {
                        script{
                            timeout(2000) {
                                try{
                                    println "LOG: PROCEDURE_SINTAX_CHECK -  ${buildEnv.PROCEDURE_SINTAX_CHECK}"
                                    if(buildEnv.PROCEDURE_SINTAX_CHECK.trim().equals("true")){
                                        println "vrunner syntaxCheck"
                                        syntaxCheck(buildEnv, connectionString) 
                                        junit allowEmptyResults: true, testResults: 'out/junit/syntaxCheck.xml'
                                    } 
                                } catch (err) {
                                    currentBuild.result = 'FAILURE'
                                }
                            }                                                 
                        }
                    }
            }
            
            stage('Расчет цикломатической сложности кода'){
                    timestamps {
                        script{
                            timeout(2000) {
                                try{
                                    if(PROCEDURE_BDD_TEST.trim().equals("true")){
                                        println "cyclo"
                                        cmdRun("oscript ./tools/cyclo.os ./src/ ./out/junit/ccm-result.xml ./out/junit/checkstyle-result.xml ./out/junit/ccm.txt")
                                        }
                                } catch (err) {
                                    currentBuild.result = 'FAILURE'
                                }
                            }
                        } 
                        
                    }
            }

            stage('Дымовое тестирование'){
                    timestamps {
                        script{
                            timeout(2000) {
                                try{
                                    println "LOG: PROCEDURE_SINTAX_CHECK -  ${PROCEDURE_TDD_TEST}"
                                    if(PROCEDURE_TDD_TEST.trim().equals("true")){
                                        tddTesting(buildEnv, connectionString)
                                    }
                                } catch (err) {
                                    currentBuild.result = 'FAILURE'
                                }
                            }                             
                        }
                        
                    }
            }
            stage('junit Отчет'){
                    timestamps {
                        script{
                            timeout(2000) {
                                try{
                                    println "junit"
                                    if(PROCEDURE_TDD_TEST.trim().equals("true")){
                                        junit 'out/junit/*.xml'
                                    }
                                } catch (err) {
                                    currentBuild.result = 'FAILURE'
                                }
                            }                             
                        }
                        
                    }
            }

            stage('Функциональное тестирование'){
                    timestamps {
                        script{
                            timeout(2000) {
                                try{
                                    if(PROCEDURE_BDD_TEST.trim().equals("true")){
                                        bddTesting(buildEnv, connectionString)
                                    }
                                } catch (err) {
                                    currentBuild.result = 'FAILURE'
                                }
                            }
                        }                   
                    }
            }
            
            stage('Публикауия результатов BDD тестирования в allure'){
                    timestamps {
                        script{
                            timeout(2000) {
                                try{
                                    if(PROCEDURE_BDD_TEST.trim().equals("true")){
                                        println "allure"
                                       // allure includeProperties: false, jdk: '', report: 'out/allure-report', results: [[path: 'out/allure']]
                                        allure includeProperties: false, jdk: '', results: [[path: 'out/allure']]
                                    }
                                } catch (err) {
                                    currentBuild.result = 'FAILURE'
                                }
                            }
                        } 
                    }
            }
            
            stage('Публикауия результатов BDD тестирования в cucumber'){
                    timestamps {
                        script{
                            timeout(2000) {
                                try{
                                    if(PROCEDURE_BDD_TEST.trim().equals("true")){
                                        println "cucumber"
                                        cucumber fileIncludePattern: '*.json', jsonReportDirectory: 'out/cucumber'
                                    }
                                } catch (err) {
                                    currentBuild.result = 'FAILURE'
                                }
                            }
                        } 
                    }
            }

            stage('Публикауия Документации в pickles'){
                    timestamps {
                        script{
                            timeout(2000) {
                                try{
                                    if(PROCEDURE_BDD_TEST.trim().equals("true")){
                                        println "pickles"
                                        cmdRun("pickles -f features -o out/pickles -l ru --df dhtml --sn 'TEST CONFIGURATION' ")                                    }
                                } catch (err) {
                                    currentBuild.result = 'FAILURE'
                                }
                            }
                        } 
                        
                    }
            }

            stage('publishHTML Документации в pickles'){
                    timestamps {
                        script{
                            timeout(2000) {
                                try{
                                    if(PROCEDURE_BDD_TEST.trim().equals("true")){
                                        println "publishHTML"
                                        publishHTML (target: [
                                            allowMissing: false, 
                                            alwaysLinkToLastBuild: false, 
                                            keepAll: true, 
                                            reportDir: 'out/pickles', 
                                            reportFiles: 'Index.html', 
                                            reportName: 'Pickles Report', 
                                            reportTitles: 'Pickles Report: TEST CONFIGURATION' 
                                            ])
                                        }
                                } catch (err) {
                                    currentBuild.result = 'FAILURE'
                                }
                            }
                        } 
                        
                    }
            }

            stage('Сборка living Docs'){
                    timestamps {
                        script{
                            timeout(2000) {
                                try{
                                    if(PROCEDURE_BDD_TEST.trim().equals("true")){
                                        println "living Docs"
    //                                    livingDocs featuresDir: 'out/cucumber'
                                    }
                                } catch (err) {
                                    currentBuild.result = 'FAILURE'
                                }
                            }
                        }                   
                    }
            }

            stage ('CheckStyle Отчет') {
                def checkstyle = scanForIssues tool: [$class: 'CheckStyle']
                //, pattern: 'out/junit/checkstyle-result.xml'
                publishIssues issues:[checkstyle]
            }

        stage('АПК') {
            
                timestamps {
                    script {
                       // def cmd_properties = "\"acc.propertiesPaths=${ACC_PROPERTIES};acc.catalog=${CURRENT_CATALOG};acc.sources=${SRC};acc.result=${TEMP_CATALOG}\\acc.json;acc.projectKey=${PROJECT_KEY};acc.check=${ACC_check};acc.recreateProject=${ACC_recreateProject}\""
                       // cmdRun("runner run --ibconnection /F${ACC_BASE} --db-user ${ACC_USER} --command ${cmd_properties} --execute \"${BIN_CATALOG}acc-export.epf\" --ordinaryapp=1")
                        cmdRun("call runner run --db-user Admin --ibconnection /FC:/sonar/acc --command \"acc.propertiesPaths=./acc.properties;acc.catalog=${CURRENT_CATALOG};acc.sources=${CURRENT_CATALOG}${SRC};\" --execute \"./tools/acc-export.epf\" --ordinaryapp=1")
                    }
                }
    
            }


        stage('Сканер SonarQube') {
            timestamps {
                script {
                    //dir('Repo') {
                        withSonarQubeEnv('SonarQube-server') {
                          //  def scannerHome = tool 'SonarQubeScanner';
                            
                            bat """chcp 65001 > nul
                                dir
                                sonar-scanner
                                """
                            PROJECT_URL = "${env.SONAR_HOST_URL}/dashboard?id=TESTSS"
                            sonar_host = "${env.SONAR_HOST_URL}"
                        }
                     //   def qg = waitForQualityGate()
                    //}
                }
            }
        }                    

            stage ('SonarQube') {
                    timestamps {
                        script{
                                        println "SonarQube"
                                        //scannerHome = tool 'SonarQubeScanner'
                                        versionText = readFile encoding: 'UTF-8', file: 'src/cf/VERSION'
                                        versionValue = (versionText =~ /<VERSION>(.*)<\/VERSION>/)[0][1]

                                        configurationText = readFile encoding:	'UTF-8' , file:	'src/cf/Configuration.xml'
                                        println "configurationText - ${configurationText}" 
                                        configurationVersion = (configurationText =~ /<Version>(.*)<\/Version>/)[0][1]
                                        // <Version>1.0.0.2</Version>
                                        println "configurationVersion - ${configurationVersion}" 
                            bat """chcp 65001 > nul
                                dir
                                sonar-scanner -Dsonar.projectVersion=${configurationVersion}
                                """                       
                        } 
                    }            
            }



            stage('Сборка поставки'){
                    timestamps {
                        script{
                            timeout(2000) {
                                try{
                                    if(currentBuild.result != 'FAILURE'){
                                        buildRelise(buildEnv, connectionString)
                                    }
                                } catch (err) {
                                    currentBuild.result = 'FAILURE'
                                }
                            }
                        }                   
                    }
            }
        
    
}

def call(){
    call([:])  
}

// Подготавливаем тестовую базу к работе
def prepareBase(Map buildEnv, String connectionString){
    if (fileExists("${buildEnv.PATH_TO_TAMPLATE_BASE}")) { 
        println "LOG: tamplate DB file exist"
        cmdRun("vrunner init-dev --ibconnection ${connectionString} --dt ${buildEnv.PATH_TO_TAMPLATE_BASE} %userCredentionalID% --v8version ${buildEnv.V8VERSION}", getDBUserCredentialsId())
        cmdRun("vrunner compile --ibconnection ${connectionString} --src=${buildEnv.SOURCE_PATH} %userCredentionalID% -c --noupdate --ibconnection ${connectionString}  --v8version ${buildEnv.V8VERSION}" , getDBUserCredentialsId())
        cmdRun("vrunner updatedb --ibconnection ${connectionString} %userCredentionalID%  --v8version ${buildEnv.V8VERSION}" , getDBUserCredentialsId())
    } else {
        cmdRun("vrunner init-dev --src=${buildEnv.SOURCE_PATH} %userCredentionalID% --v8version ${buildEnv.V8VERSION}" , getDBUserCredentialsId())
    }  
    cmdRun("vrunner run --ibconnection ${connectionString} %userCredentionalID% --command 'ЗапуститьОбновлениеИнформационнойБазы;ЗавершитьРаботуСистемы;' --execute \$runnerRoot/epf/ЗакрытьПредприятие.epf" , getDBUserCredentialsId())

    if (fileExists('compile.log')) {
        archiveArtifacts 'compile.log'
    }
}

def syntaxCheck(Map buildEnv, String connectionString) { 
    println "vrunner syntax-check %userCredentionalID%  --junitpath ./out/junit/syntaxCheck.xml --ibconnection ${connectionString}  --v8version ${buildEnv.V8VERSION}"  
    cmdRun("vrunner syntax-check %userCredentionalID%  --junitpath ./out/junit/syntaxCheck.xml --ibconnection ${connectionString}  --v8version ${buildEnv.V8VERSION}" , getDBUserCredentialsId())
}

// Дымовое тестирование (BDD)
def tddTesting(Map buildEnv, String connectionString){
    cmdRun("vrunner xunit %userCredentionalID% --settings ${buildEnv.VRUNNER_CONF} --ibconnection ${connectionString} --v8version ${buildEnv.V8VERSION}  --testclient ::1538" , getDBUserCredentialsId())
    if (fileExists('log-xunit.txt')) {
        archiveArtifacts 'log-xunit.txt'
    }
}

// Vanessa-Add 
def bddTesting(Map buildEnv, String connectionString){
    cmdRun("runner vanessa %userCredentionalID% --settings ${buildEnv.VRUNNER_CONF}  --ibconnection ${connectionString} --v8version ${buildEnv.V8VERSION}", getDBUserCredentialsId())  

    if (fileExists('vbOnline.log')) {
        archiveArtifacts 'vbOnline.log'
    }
// vbOnline.log
}



def buildRelise(Map buildEnv, String connectionString){
    cmdRun("packman set-database ${connectionString} %userCredentionalID%", getDBUserCredentialsId())
    cmdRun("packman make-cf -v8version ${buildEnv.V8VERSION}")
    
    if (fileExists('.packman/1cv8.cf')) {
        archiveArtifacts '.packman/1cv8.cf'
    }
}

def getDBUserCredentialsId(Map buildEnv) {
    try{
        DB_USER_CREDENTIONALS_ID = _DB_USER_CREDENTIONALS_ID
    println "${DB_USER_CREDENTIONALS_ID}"    
        return "${DB_USER_CREDENTIONALS_ID}"
    } catch (err) {
        return ""
    }
}
