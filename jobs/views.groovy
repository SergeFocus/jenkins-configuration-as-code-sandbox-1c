listView('Snapshots') {
  recurse(true)
  columns {
    jobName()
    status()
    buildButton()
  }
  jobs {
    regex('.*snapshot$')
  }
}

listView('Releases') {
  recurse(true)
  columns {
    jobName()
    status()
    buildButton()
  }
  jobs {
    regex('.*release$')
  }
}

listView('Merge Requests') {
  recurse(true)
  columns {
    jobName()
    status()
    buildButton()
  }
  jobs {
    regex('.*merge-request$')
  }
}

listView('GitSynk') {
  recurse(true)
  columns {
    jobName()
    status()
    buildButton()
  }
  jobs {
    regex('.*GitSynk$')
  }
}

listView('CI-CD 1C') {
  recurse(true)
  columns {
    jobName()
    status()
    buildButton()
  }
  jobs {
    regex('.*CICD_1C$')
  }
}

listView('Code Quality 1C') {
  recurse(true)
  columns {
    jobName()
    status()
    buildButton()
  }
  jobs {
    regex('.*Code_Quality_1C$')
  }
}

listView('Pipeline 1C') {
  recurse(true)
  columns {
    jobName()
    status()
    buildButton()
  }
  jobs {
    regex('.*Pipeline1C$')
  }
}